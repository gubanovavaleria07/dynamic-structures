﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace realization
{
    public record Student(string name, int group, string exam, double grade);
    public class ClassStudent 
    {
        public static IEnumerable<string> Student_MaximumScore(List<Student> List) {
            var max = List
            .GroupBy(
                student => student.name,
                student => student.grade,
                (studentName, studentGrade) => new {
                    Key = studentName,
                    Average = studentGrade.Average(),
                }
            ).GroupBy(
                student => Math.Round(student.Average,2),
                student => student.Key,
                (AverageRound, Names) => new {
                    Key = AverageRound,
                    Name = Names,
                }
            ).OrderByDescending(student => student.Key).First().Name;

            return max;
        }

        public static Dictionary<string,double> Average_Score_Exam(List<Student> List) {
            var average = List 
            .GroupBy(
                exam => exam.exam,
                student => student.grade,
                (Exam, StudentGrade) => new {
                    Key = Exam,
                    Grades = Math.Round(StudentGrade.Average(),2)
                }
            ).ToDictionary(Exam => Exam.Key, Grade => Grade.Grades);
            return average;
        }

        public static Dictionary<string,List<int>> Best_Group_for_Lesson(List<Student> List) {
            var best = List 
            .GroupBy(
                student => new {Exams = student.exam, Groups = student.group},
                student => student.grade,
                (ExamAndGroups, studentGrade) => new {
                    Key = new {Exam = ExamAndGroups.Exams, Group = ExamAndGroups.Groups},
                    Average = Math.Round(studentGrade.Average(),2)
                    }
                )
            .GroupBy(
                student => new {Grades = student.Average, Exams = student.Key.Exam},
                student => student.Key.Group,
                (GradeAndExam, studentKeyGroup) => new {
                    Exam = GradeAndExam.Exams,
                    KeyGroup = new {studentKeyGroup, GradeAndExam.Grades}, 
                    } 
            ).GroupBy(
                student => student.Exam,
                student => student.KeyGroup,
                (studentExam, studentKey) => new {
                    Exam = studentExam,
                    Best = studentKey.OrderByDescending(s => s.Grades).First().studentKeyGroup
                    }
            ).ToDictionary(s => s.Exam, s => s.Best.ToList<int>());

            return best;
        }
    }
    
}
