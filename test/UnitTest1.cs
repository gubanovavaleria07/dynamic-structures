using Xunit;
using System.Collections.Generic;
using realization;

namespace test
{
    public class UnitTest1
    {
        [Fact]
        public void Test_Student_MaximumScore()
        {
           List <Student> table = new List<Student> {
               new Student ("Name1", 1, "Programming", 2),
               new Student ("Name1", 1, "Math", 4),
               new Student ("Name2", 2, "Physics", 3),
               new Student ("Name2", 2, "Programming", 4),
               new Student ("Name3", 1, "Programming", 5),
               new Student ("Name3", 1, "Math", 5),
               new Student ("Name4", 3, "Programming", 4),
               new Student ("Name4", 3, "Math", 4),
               new Student ("Name4", 3, "Physics", 5),
               new Student ("Name5", 2, "Programming", 2)
           };
           Assert.Equal(new List<string> {"Name3"}, ClassStudent.Student_MaximumScore(table));
        }

        [Fact]
        public void Test_Average_Score_Exam() {
           
           List <Student> table = new List<Student> {
               new Student ("Name1", 1, "Programming", 2),
               new Student ("Name1", 1, "Math", 4),
               new Student ("Name2", 2, "Physics", 3),
               new Student ("Name2", 2, "Programming", 4),
               new Student ("Name3", 1, "Programming", 5),
               new Student ("Name3", 1, "Math", 5),
               new Student ("Name4", 3, "Programming", 4),
               new Student ("Name4", 3, "Math", 4),
               new Student ("Name4", 3, "Physics", 5),
               new Student ("Name5", 2, "Programming", 2)
           };
           
            Dictionary <string,double> rez = new Dictionary<string,double>() {
                {"Math", 4.33},
                {"Programming", 3.4},
                {"Physics", 4}
            };
            Assert.Equal(rez, ClassStudent.Average_Score_Exam(table));
        }

        [Fact]
        public void Test_Best_Group_for_Lesson() {
            List <Student> table = new List<Student> {
               new Student ("Name1", 1, "Programming", 2),
               new Student ("Name1", 1, "Math", 4),
               new Student ("Name2", 2, "Physics", 3),
               new Student ("Name2", 2, "Programming", 4),
               new Student ("Name3", 1, "Programming", 5),
               new Student ("Name3", 1, "Math", 5),
               new Student ("Name4", 3, "Programming", 4),
               new Student ("Name4", 3, "Math", 4),
               new Student ("Name4", 3, "Physics", 5),
               new Student ("Name5", 2, "Programming", 2)
           };
           Dictionary<string,List<int>> rez = new Dictionary<string, List<int>> {
               {"Programming", new List<int> {3}},
               {"Math", new List<int> {1}}, 
               {"Physics", new List<int> {3}}
               };
               
               Assert.Equal(rez, ClassStudent.Best_Group_for_Lesson(table));
           
        }
    }
}

